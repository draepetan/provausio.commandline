﻿namespace Provausio.CommandLine.Arguments.Lexing
{
    internal enum CharacterType
    {
        QUOTE  = 0x22,
        MARKER = 0x2d,
        VALUE,
        SPACE  = 0x20,
        EQUALS = 0x3d
    }
}