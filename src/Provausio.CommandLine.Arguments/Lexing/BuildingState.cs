﻿namespace Provausio.CommandLine.Arguments.Lexing
{
    internal enum BuildingState
    {
        STARTING,
        BUILDING_VERB,
        BUILDING_ARGUMENT,
        BUILDING_ARGUMENT_VALUE
    }
}