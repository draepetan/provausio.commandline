﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Provausio.CommandLine.Arguments.Lexing;

namespace Provausio.CommandLine.Arguments
{
    public class Arguments
    {
        private static readonly ArgumentParser ArgumentParser;

        static Arguments()
        {
            ArgumentParser = new ArgumentParser();
        }

        public static IDictionary<string, object> Parse(string[] args)
        {
            var argsString = string.Join(" ", args);
            return ArgumentParser
                .Read(argsString)
                .ToDictionary(k => k.Key, v => v.Value);
        }

        public static T Parse<T>(string[] args)
            where T : class, new()
        {
            var arguments = Parse(args);
            var options = new T();

            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                var attrs = property.GetCustomAttributes(true);
                foreach (var attr in attrs)
                {
                    if (!(attr is OptionAttribute option)) continue;

                    object arg = null;
                    if (arguments.ContainsKey(option.LongName))
                        arg = Convert.ChangeType(arguments[option.LongName], property.PropertyType);
                    else if (arguments.ContainsKey(option.ShortName.ToString()))
                        arg = Convert.ChangeType(arguments[option.ShortName.ToString()], property.PropertyType);

                    if (arg == null)
                    {
                        if(option.Required)
                            throw new ArgumentException($"{option.ShortName}/{option.LongName} is a required argument!");
                        continue;
                    }

                    property.SetValue(options, arg);
                }
            }

            return options;
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class OptionAttribute : Attribute
    {
        public char ShortName { get; set; }

        public string LongName { get; set; }

        public bool Required { get; set; }

        public string Description { get; set; }

        public OptionAttribute(char shortName, string longName)
        {
            ShortName = shortName;
            LongName = longName;
        }
    }
}
