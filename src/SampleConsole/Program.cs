﻿using System;
using Provausio.CommandLine.Arguments;

namespace SampleConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (var argument in Arguments.Parse(args))
            {
                Console.WriteLine($"{argument.Key} = {argument.Value}");
            }
        }
    }
}
