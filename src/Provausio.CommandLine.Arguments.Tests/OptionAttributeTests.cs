﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Provausio.CommandLine.Arguments.Tests
{
    public class OptionAttributeTests
    {
        [Fact]
        public void ParseT_SunnyDay_AllValuesLoaded()
        {
            // arrange
            var args = new[]{"-a MyApp --env=development -d"};

            // act
            var options = Arguments.Parse<TestOptions>(args);

            // assert
            Assert.Equal("MyApp", options.ApplicationName);
            Assert.Equal("development", options.Environment);
            Assert.True(options.DoThing);
        }

        [Fact]
        public void ParseT_MissingRequired_Throws()
        {
            // arrange
            var args = new[] { "--env=development -d" };

            // act

            // assert
            Assert.Throws<ArgumentException>(() => Arguments.Parse<TestOptions>(args));
        }
    }
    
    internal class TestOptions
    {
        [Option('a', "app", Required = true, Description = "The name of the application")]
        public string ApplicationName { get; set; }

        [Option('d', "doThing", Description = "Do the thing")]
        public bool DoThing { get; set; }

        [Option('e', "env", Required = true, Description = "The environment")]
        public string Environment { get; set; }
    }
}
