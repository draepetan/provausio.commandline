﻿using System;
using System.Linq;
using Provausio.CommandLine.Arguments.Lexing;
using Xunit;

namespace Provausio.CommandLine.Arguments.Tests
{
    public class ArgumentLexerTests
    {
        private readonly ArgumentParser _parser;

        public ArgumentLexerTests()
        {
            _parser = new ArgumentParser();
        }

        [Theory]
        [InlineData("verb -n dev --a=app-name -flag -x foo")]
        [InlineData("verb --a=app-name -n dev -flag -x foo")]
        [InlineData("verb --a=app-name -flag -n dev -x foo")]
        [InlineData("verb --a=app-name -x foo -flag -n dev")]
        [InlineData("verb -flag --a=app-name -x foo -n dev")]
        public void Read_AllArgumentsFound(string args)
        {
            // arrange
            
            // act
            var result = _parser.Read(args).ToDictionary(
                k => k.Key,
                v => v.Value);

            // assert
            Assert.Equal("dev", result["n"]);
            Assert.Equal("app-name", result["a"]);
            Assert.Equal("foo", result["x"]);
            Assert.True(result.ContainsKey("flag"));
        }


        [Theory]
        [InlineData("verb1 -n dev", "verb1")]
        [InlineData("verb1 verb2 -n dev", "verb1", "verb2")]
        [InlineData("verb1 verb2 verb3 -n dev", "verb1", "verb2", "verb3")]
        public void Read_MultipleVerbs_DoParse(string args, params string[] expectedVerbs)
        {
            // arrange

            // act
            var result = _parser.Read(args).ToDictionary(
                k => k.Key,
                v => v.Value);

            // assert
            foreach (var verb in expectedVerbs)
            {
                Assert.True(result.ContainsKey(verb));
            }
        }

        [Theory]
        [InlineData("-n dev verb1")]
        [InlineData("verb1 -n dev verb2 ")]
        [InlineData("verb1 verb2 -n dev verb3")]
        public void Read_VerbOutOfOrder_Throws(string args)
        {
            // arrange

            // act
            Assert.Throws<FormatException>(() => _parser.Read(args).ToList());
        }

        [Theory]
        [InlineData("-n \"foo bar\" --baz=\"hello world!\" -x bat")]
        public void Read_QuotedFields_CapturedVerbatim(string args)
        {
            // arrange

            // act
            var result = _parser.Read(args).ToDictionary(k => k.Key, v => v.Value);

            // assert
            Assert.Equal("foo bar", result["n"]);
            Assert.Equal("hello world!", result["baz"]);
        }

        [Fact]
        public void Read_TooManyDashes_Throws()
        {
            // arrange
            var args = "-n thing --app=some-name ---foo=bar";

            // act

            // assert
            Assert.Throws<FormatException>(() => _parser.Read(args).ToList());
        }

        [Fact]
        public void Read_DataOutsideOfQuotes_Throws()
        {
            // arrange
            var args = "-n foo --app=\"some name\"bar";

            // act

            // assert
            Assert.Throws<FormatException>(() => _parser.Read(args).ToList());
        }

        [Fact]
        public void Read_Shortname_TooLong_Throws()
        {
            // arrange
            var args = "-n foo -bar baz";

            // act

            // assert
            Assert.Throws<FormatException>(() => _parser.Read(args).ToList());
        }
    }
}
